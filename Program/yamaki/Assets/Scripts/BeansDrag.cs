﻿using UnityEngine;
using System.Collections;

public class BeansDrag : MonoBehaviour {

    //-------------------------------------
    //ドラッグ＆ドロップで移動
    //削除は右クリック
    //（余裕があれば升にドラッグ＆ドロップ）
    //-------------------------------------

    private float mPositionZ = 10.0f;       //Z座標固定
    //private Vector2 mBasePoint;
    private SpriteRenderer mMasuSprite;
    private Vector2 mMasuPosition;

    //private float mMasuWidth;
    //private float mMasuHeight;
    //private Collider2D mMasuCol;

    public Collider2D mRakkaseiCol;

    private void Start()
    {
        mMasuSprite = gameObject.GetComponent<SpriteRenderer>();
        mMasuPosition = mMasuSprite.transform.position;

        //mMasuWidth = mMasuSprite.bounds.size.x;
        //mMasuHeight = mMasuSprite.bounds.size.y;
        //升のコライダーを取得
        //mMasuCol = gameObject.GetComponent<Collider2D>();

        //落花生のコライダーを取得
        mRakkaseiCol = gameObject.GetComponent<Collider2D>();
    }

    public void Update()
    {
        //右クリックで豆を消す
        if (Input.GetMouseButtonDown(1))
        {
            Vector2 mBeanClickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D mCol2D = Physics2D.OverlapPoint(mBeanClickPoint);

            if (mCol2D != null && mCol2D == mRakkaseiCol)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void OnMouseDrag()
    {
        //Instantiate(this.Rakkasei, new Vector3(0, 5, 0), Quaternion.identity);
        //元の座標を保存
        //mBasePoint = transform.position;
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mPositionZ));
        //Debug.Log(mBasePoint);
        //Debug.Log(Input.mousePosition.y);
    }

    //升の上でドロップすると消える(途中)
    //public void OnMouseUp()
    //{
    //    //transform.position = mBasePoint;

    //    if ((mMasuPosition.x <= Input.mousePosition.x && Input.mousePosition.x <= mMasuPosition.x + mMasuWidth) &&
    //        (mMasuPosition.y <= Input.mousePosition.y && Input.mousePosition.y <= mMasuPosition.y + mMasuHeight))
    //    {
            
    //        Debug.Log("ゴミ箱の上");
    //        Destroy(this.gameObject);
    //    }
    //}

    //升に乗せると消える(途中)
    //public void OnMouseOver()
    //{
    //    //transform.position = mBasePoint;

    //    if ((mMasuPosition.x <= Input.mousePosition.x && Input.mousePosition.x <= mMasuPosition.x + mMasuWidth) &&
    //        (mMasuPosition.y <= Input.mousePosition.y && Input.mousePosition.y <= mMasuPosition.y + mMasuHeight))
    //    {
    //        Debug.Log("ゴミ箱の上");
    //        DestroyObject(this);
    //    }
    //}
}
