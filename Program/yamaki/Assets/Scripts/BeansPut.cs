﻿using UnityEngine;
using System.Collections;

public class BeansPut : MonoBehaviour
{
    //-----------------------------
    //右の豆の画像をクリックすると
    //別の場所に複製される
    //(複製される座標は仮)
    //-----------------------------


    private float mPositionZ = 10.0f;       //Z座標固定
    public Collider2D mRakkaseiCol;
    public GameObject mRakkaseiPrefab;

	public void Start()
    {
        //落花生のコライダーを取得
        mRakkaseiCol = gameObject.GetComponent<Collider2D>();
        //落花生プレハブを取得
        mRakkaseiPrefab = (GameObject)Resources.Load("Prefabs/Rakkasei");
	}

	public void Update()
    {
        //左クリック
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mBeanClickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D mCol2D = Physics2D.OverlapPoint(mBeanClickPoint);

            if((mCol2D != null) && (mCol2D == mRakkaseiCol))
            {
                //複製する(座標は仮)
                Instantiate(mRakkaseiPrefab, new Vector3( Screen.width / 2 - 100, Screen.height / 2, mPositionZ ), Quaternion.identity);
                //Debug.Log("複製");
            }
        }
	}
}
