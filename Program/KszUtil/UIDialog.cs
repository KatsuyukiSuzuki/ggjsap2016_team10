﻿using System.Collections;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Events;

public class UIDialog : MonoBehaviour
{
    //自分の配下にあるButtonを全部集めて外に置く
    private DialogButtonEvent _onButton;
    public CanvasGroup canvas;
    public DialogButtonEvent OnButton { get { return _onButton ?? (_onButton = new DialogButtonEvent()); } }
    public UnityEvent OnClosed = new UnityEvent();
    private Coroutine coroutine;

    public virtual void OnClose()
    {
    }

    public IEnumerator AwaitClose()
    {
        yield return new WaitWhile(() => IsShow);
    }

    public bool IsShow
    {
        private set
        {
            canvas.interactable = value;
            isShow = value;
        }
        get
        {
            return isShow;
        }
    }

    private bool isShow;

    public void Awake()
    {
        var buttons = GetComponentsInChildren<DialogButton>(true);
        foreach (var dialogButton in buttons)
        {
            var btn = dialogButton;
            btn.Button.onClick.AddListener(() => OnButtonClick(btn));
        }
    }

    public virtual void OnButtonClick(DialogButton btn)
    {
        //何か演出中だったらボタンが反応するのはやめさせる
        if (coroutine == null)
        {
            OnButton.Invoke(this, btn);
        }
    }


    public void Start()
    {
        canvas.alpha = 0.0f;
        canvas.blocksRaycasts = false;
    }

    public void Reset()
    {
        canvas = GetComponent<CanvasGroup>();
    }

    public UnityEvent Show(bool showFlag = true)
    {
        if (showFlag != IsShow)
        {
            if (showFlag)
            {
                IsShow = true;
            }
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
            coroutine = StartCoroutine(ShowIterator(showFlag));
        }
        return OnClosed;
    }

    private IEnumerator ShowIterator(bool showFlag)
    {
        float showSpeed = showFlag ? 3.0f : -3.0f;
        while (true)
        {
            canvas.alpha += Time.deltaTime * showSpeed;
            if (showFlag)
            {
                canvas.blocksRaycasts = true;
                if (canvas.alpha >= 1.0f)
                {
                    canvas.alpha = 1.0f;
                    break;
                }
            }
            else
            {
                if (canvas.alpha <= 0.0f)
                {
                    canvas.alpha = 0.0f;
                    canvas.blocksRaycasts = false;
                    IsShow = false;
                    OnClose();
                    OnClosed.Invoke();
                    OnClosed.RemoveAllListeners();
                    break;
                }
            }
            yield return null;
        }
        coroutine = null;
    }
}