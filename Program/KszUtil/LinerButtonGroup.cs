﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class LinerButtonEvent : UnityEvent<int>{}

public class LinerButtonGroup : MonoBehaviour
{
    public Button[] buttons;
    public bool isLoop = true;

    [SerializeField]
    private LinerButtonEvent onCurrentButtonIDChanged;
    public LinerButtonEvent OnCurrentButtonIDChanged { get { return onCurrentButtonIDChanged ?? (onCurrentButtonIDChanged = new LinerButtonEvent()); } }

    private int _currentButtonID;

    public void Next()
    {
        Set(_currentButtonID + 1);
    }

    public void Prev()
    {
        Set(_currentButtonID - 1);
    }

    public void Set(int buttonId)
    {
        if (buttonId >= buttons.Length)
        {
            buttonId = isLoop ? 0 : (buttons.Length - 1);
        }
        if (buttonId < 0)
        {
            buttonId = isLoop ? (buttons.Length - 1) : 0;
        }
        _currentButtonID = buttonId;
        OnCurrentButtonIDChanged.Invoke(buttonId);
        for (int index = 0; index < buttons.Length; index++)
        {
            var button = buttons[index];
            if (index == buttonId)
            {
                button.gameObject.SetActive(true);
            }
            else
            {
                button.gameObject.SetActive(false);
            }
        }
    }

    protected void Start () {
        foreach (var button in buttons)
        {
            button.onClick.AddListener(() => Next());
        }
	}
}
