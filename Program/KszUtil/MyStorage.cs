﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class MyStorage
{
    public MyStorage(string basePath, string path)
    {
        BasePath = Path.Combine(basePath, path);
        Directory.CreateDirectory(BasePath);
    }

    public string BasePath {private set; get; }

    private Stream OpenRead(string path)
    {
        //このままだと、WebPlayer対応出来ない。　まぁ出来なくても良い気もするんだけれど。
        var fullPath = Path.Combine(BasePath, path);
        if (File.Exists(fullPath))
        {
            return File.OpenRead(fullPath);
        }
        return Stream.Null;
    }

    public Stream OpenWrite(string path)
    {
        var fullPath = Path.Combine(BasePath, path);
        return File.Open(fullPath, FileMode.Create);
    }   

    public void Serialize<T>(T data,string dataName)
    {
        using (var stream = OpenWrite("save" + dataName + ".dat"))
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            xmlSerializer.Serialize(stream, data);
        }
    }

    public T Deserialize<T>(string dataName,out T data)
    {
        using (var stream = OpenRead("save" + dataName + ".dat"))
        {
            if (stream == Stream.Null)
            {
                data = default(T);
                return data;
            }
            var xmlSerializer = new XmlSerializer(typeof(T));
            data = (T)xmlSerializer.Deserialize(stream);
            return data;
        }
    }
}