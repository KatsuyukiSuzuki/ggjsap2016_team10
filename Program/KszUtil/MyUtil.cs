﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class MyUtil : SingletonMonoBehaviour<MyUtil> {
    public Coroutine Coroutine { private set; get; }

    public void DelayFunc(int frame, Action action) {
		Coroutine = StartCoroutine(_DelayFunc(frame, action));
	}

	private IEnumerator _DelayFunc(int frame, Action action) {
		for (int i = 0; i < frame; ++i) yield return null;
		action();
	    Coroutine = null;
	}

    //渡された重み付け配列からIndexを得る
    public static int GetRandomIndex(params int[] weightTable)
    {
        var totalWeight = weightTable.Sum();
        var value = Random.Range(1, totalWeight + 1);
        var retIndex = -1;
        for (var i = 0; i < weightTable.Length; ++i)
        {
            if (weightTable[i] >= value)
            {
                retIndex = i;
                break;
            }
            value -= weightTable[i];
        }
        return retIndex;
    }

    private Texture2D screenShotTexture;
    public Texture2D ScreenShotTexture { get { return screenShotTexture ?? (screenShotTexture = new Texture2D(Screen.width,Screen.height,TextureFormat.RGB24, false)); } }
    private Coroutine coroutine;
    public void CaptureScreen()
    {
        if (coroutine != null) return;
        coroutine = StartCoroutine(_CaptureFrame());
    }
    private IEnumerator _CaptureFrame()
    {
        int splitCount = 1;
        var splitW = Screen.width / splitCount;
        var splitH = Screen.height / splitCount;
        for (int x = 0; x < splitCount; ++x)
        {
            for (int y = 0; y < splitCount; ++y)
            {
                yield return new WaitForEndOfFrame();
                ScreenShotTexture.ReadPixels(new Rect(x * splitW, y * splitH, splitW, splitH), x * splitW, y * splitH, false);
            }
        }
        yield return null;
        ScreenShotTexture.Apply(false);
        coroutine = null;
    }

    public void SaveLastScreenCapture(string savePath)
    {
        var bytes = ScreenShotTexture.EncodeToPNG();
        File.WriteAllBytes(savePath, bytes);
    }

    private static float sTime;
    public static void TimeMeasureStart()
    {
        sTime = Time.realtimeSinceStartup;
    }
    public static float TimeMeasureEnd(bool isDebugLog = false)
    {
        var t = sTime;
        sTime = Time.realtimeSinceStartup;
        if (isDebugLog)
        {
            Debug.Log("time:" + (sTime-t));
            sTime = Time.realtimeSinceStartup;
        }
        return sTime - t;
    }
}

public static class LinqExtensions
{
    public static T RandomAt<T>(this IEnumerable<T> ie)
    {
        if (ie.Any() == false) return default(T);
        return ie.ElementAt(Random.Range(0, ie.Count()));
    }
}
